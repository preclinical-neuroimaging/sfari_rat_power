Plot Shank3-/- SFARI 2021
================
Joanes Grandjean

``` r
#install rainclouds
#devtools::install_github('jorvlan/raincloudplots')

library(tidyverse) 
library(raincloudplots)
library(glue)
library(stringr)
```

``` r
analysis_folder<-'/project/4180000.24/SFARI_2021/analysis'
study<-read_csv('/project/4180000.24/SFARI_2021/study.csv')
```

``` r
for(i in 1:16){
 
  IC<- glue('IC{str_pad(i, 4, pad = "0")}')
  study <- study %>% add_column(!!IC := 0)
  file_list <-  IC %>% list.files(path = analysis_folder, pattern=., recursive = TRUE) %>%  .[!str_detect(., ".nii.gz")] 
  
  for(file_name in file_list){
    id<-str_split(file_name,'/') %>% unlist %>% .[1] %>% str_split('-') %>% unlist() %>% .[2] %>% as.numeric()
    study[which(study$ID == id), IC] <- read_file(file.path(analysis_folder,file_name)) %>% as.numeric()  
  }
}
```

``` r
df_1x1 <- data_1x1(
  array_1 = study$IC0007[study$group=='WT'][3:11],
  array_2 = study$IC0007[study$group=='MU'],
  jit_distance = .09,
  jit_seed = 321)


raincloud_1_h <- raincloud_1x1(
  data = df_1x1, 
  colors = (c('dodgerblue','darkorange')), 
  fills = (c('dodgerblue','darkorange')), 
  size = 1, 
  alpha = .6, 
  ort = 'h') +

scale_x_continuous(breaks=c(1,2), labels=c("Wild-type", "Shank3-/-"), limits=c(0, 3)) +
  xlab("Groups") + 
  ylab("Connectivity strength [a.u.]") +
  theme_classic()

raincloud_1_h %>% ggsave('assets/raincloud.svg',plot=.,device = 'svg',dpi=300, width = 200, height = 150, units = 'mm')
raincloud_1_h %>% ggsave('assets/raincloud.png',plot=.,device = 'png',dpi=300, width = 200, height = 150, units = 'mm')
```

![figure raincloud](assets/raincloud.png)
